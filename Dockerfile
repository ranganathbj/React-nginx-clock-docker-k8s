
FROM node:13.12.0-alpine as build-step

RUN mkdir -p /app
ENV PATH="./node_modules/.bin:$PATH"

WORKDIR /app

COPY package.json /app
RUN npm install
COPY . .

EXPOSE 8080
RUN npm run build


# Stage 2

#FROM nginx:1.17.1-alpine

#COPY --from=build-step /app/build /usr/share/nginx/html

FROM nginx:1.17-alpine

RUN apk --no-cache add curl
#RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst.exe.exec `uname -s`-`uname -m` -o envsubst && \
 #   sudo chmod +x envsubst.exe && \
  #  sudo mv envsubst.exe /usr/local/bin
COPY ./nginx.config /etc/nginx/nginx.template
#CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
CMD ["/bin/sh", "-c", "nginx -g 'daemon off;'"]
COPY --from=build-step /app /usr/share/nginx/html
